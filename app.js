var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var uuid = require('uuid');
var config = require('./config');

var routes = require('./routes/index');
var users = require('./routes/users');
var db = require('./routes/db_posts');
var login = require('./routes/login');

var app = express();

function menuHighlighter(req, res, next) {
    "use strict";
    var location = req.originalUrl;
    if (location === config.homeURL) {
        req.session.location = config.homeURL;
    } else if (location === config.formURL) {
        req.session.location = config.formURL;
    } else if (location === config.loginURL) {
        req.session.location = config.loginURL;
    } else {
        req.session.location = undefined;
    }
    next();
}

/*
function checkForLogin(req, res, next) {

}
*/

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(__dirname + '/public/Images/favicon.ico'));
app.use(session({
    secret: uuid.v1()
}));
app.use(menuHighlighter);

app.use('/', routes);
app.use('/users', users);
app.use('/api', db);
app.use('/auth', login);
// catch 404 and forward to error handler
/*app.use(function(req, res, next) {
    var err = new Error('Not');
    err.status = 404;
    next(err);
});*/

// error handlers

// development error handler
// will print stacktrace
/*if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}*/
app.use(function (req, res) {
    res.render('error404');
});
app.use(function (req, res) {
    res.render('error500');
});

/*
app.use(function(err, req, res, next) {
    res.status(err.status || 404);
    res.render('error404', {
        message: err.message,
        error: {}
    });
});
*//*
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error500', {
        message: err.message,
        error: {}
    });
});*/
module.exports = app;
