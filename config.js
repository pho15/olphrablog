var config = {};
config.vote = {};
config.vote.up = "up";
config.vote.down = "down";
config.homeURL = '/';
config.formURL = '/blogform';
config.loginURL = '/login';

module.exports = config;
