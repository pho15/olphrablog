/**
 * Created by rafael on 06.05.2015.
 */

function BlogCrud() {
    "use strict";
    this.pfc = new PostFormComponent();
    this.create = function (titleId, contentId, urlId) {
        this.pfc.initialize([titleId, contentId, urlId], "/api/link");
        this.pfc.postData("PUT", function () {
            $('#blog-form-container').html('<p>Geposted</p>');
        });
    };
    this.delete = function (id) {
        var blogData = {
            id: id
        };
        $.ajax({
            method: "DELETE",
            url: "/api/" + id,
            data: blogData
        })
            .done(function (data) {
                if (data.success) {
                    renderComponent.loadData("api/links", callback);
                } else {
                    if(!data.isLoggedIn || !data.isOwner) {
                        window.location('/auth/login');
                    } else {
                        window.location('/error500');
                    }
                }
            })
            .error(function (data) {
                window.location.replace('/error500');
            });

    };
}




