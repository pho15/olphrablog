/**
 * Created by rafael on 07.05.2015.
 */
function PostFormComponent() {
    "use strict";
    this.data = {};
    this.inputs = [];
    this.url = "";
    this.initialize = function (inputs, url) {
        this.inputs = inputs;
        this.url = url;
    };
    this.postData = function (method, callback, onError) {
        var self = this;
        $.each(this.inputs, function (index, inputId) {
            var inputField = $(inputId);
            self.data[inputField.attr('data-model-name')] =  inputField.val();
        });
        $.ajax({
            method: method,
            url: self.url,
            data: self.data
        })
            .done(function (data) {
                if (callback) {
                    callback(data);
                }
            })
            .error(function (data) {
                if (onError) {
                    onError(data);
                }
            });
    };
}