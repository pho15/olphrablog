/**
 * Created by rafael on 02.05.2015.
 */
var blogCrud = new BlogCrud();
var validateOptions = {lang: "de"};
$(document).ready(function () {
    "use strict";
    var blogForm = $('#blog-form');
    $('#post-blog-btn').on('click', function(evt){
        evt.preventDefault();
        blogForm.validate(validateOptions);
        if (blogForm.valid()) {
            blogCrud.create("#title", "#content", "#url");
            setTimeout(function () {
                window.location.replace('/');
            }, 1000);
        }
    });

    $('#cancel-post-btn').on('click', function (evt) {
        evt.preventDefault();
        window.location.replace('/');
    });

    blogForm.validate(validateOptions);
});

