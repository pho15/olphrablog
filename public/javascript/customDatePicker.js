/**
 * Created by rafael on 05.05.2015.
 */

function CustomDatePicker() {
    "use strict";
    this._hasNativeDatePicker = "notDetermined";
    this.hasNativeDatePicker = function () {
        if (this._hasNativeDatePicker === "notDetermined") {
            var i = document.createElement("input");
            i.setAttribute("type", "date");
            this._hasNativeDatePicker = i.type !== "text";
        }
        return this._hasNativeDatePicker;
    };
    this.makeCustomDatePicker = function (dateField) {
        if (!this.hasNativeDatePicker()) {
            var el = $(dateField);
            el.datepicker();
        }
    };
}

