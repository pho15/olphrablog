/**
 * Created by rafael on 05.05.2015.
 */

function BlogViewModel() {
    "use strict";
    var self = this;
    this.filterModel = {
        fromDate: "",
        toDate: "",
        searchText: ""
    };
    this.dateFormat = "YYYY-MM-DD";
    this.toDateField = "";
    this.fromDateField = "";
    this.searchTextField = "";
    this.renderComponent = new RenderBlogComponent();
    this.errorMsg = "<p>Upsala, da lief was schief</p>";
    this.active = false;
    this.initialize = function (toDateId, fromDateId, searchTextId, callback) {
        this.callback = callback;
        this.toDateField = $(toDateId);
        this.fromDateField = $(fromDateId);
        this.searchTextField = $(searchTextId);
        this.toDateField.on('input', function () {
            self.active = true;
            self.updateFilterModelAndUpdate();
        });
        this.fromDateField.on('input', function () {
            self.active = true;
            self.updateFilterModelAndUpdate();
        });
        this.searchTextField.on('input', function () {
            self.active = true;
            self.updateFilterModelAndUpdate();
        });
        this.filterModel.fromDate = this.getOldestEntryDate();
        this.filterModel.toDate = Date.now();
        this.toDateField.val(moment(Date.now()).format(this.dateFormat));
        this.filterModel.searchText = "";
    };
    this.initializeRenderComponent = function (templateId, containerId) {
        this.renderComponent.initialize(templateId, containerId);
    };

    this.getOldestEntryDate = function () {
        self = this;
        $.ajax({
            method: "GET",
            url: "api/date/oldest"
        })
            .done(function (data) {
                var date = moment(data);
                self.fromDateField.val(date.format(self.dateFormat));
                return date.format(self.dateFormat);
            })
            .error(function (data) {
                self.renderComponent.container.html = self.errorMsg;
            });
    };
    this.updateFilterModelAndUpdate = function () {

        this.filterModel.fromDate = this.fromDateField.val() || this.filterModel.fromDate;
        this.filterModel.toDate = this.toDateField.val() || this.filterModel.toDate;
        this.filterModel.searchText = this.searchTextField.val();
        this.updateData();
    };
    this.updateData =  function () {
        self = this;
        $.ajax({
            method: "POST",
            url: "/api/filtered-links",
            data: self.filterModel
        })
            .done(function (data) {
                self.renderComponent.renderData(data, self.callback);
            })
            .error(function (data) {
                self.renderComponent.container.html = self.errorMsg;
            });
    };

}
