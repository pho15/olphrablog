/**
 * Created by rafael on 27.04.2015.
 */
var renderComponent = new RenderBlogComponent();
var blogController = new BlogCrud();
var utils = new UtilsHelper();
var filterModel = new BlogViewModel();

function initiateDateFields() {
    "use strict";
    var customDatePickerFactory = new CustomDatePicker();
    customDatePickerFactory.makeCustomDatePicker("#to-date");
    customDatePickerFactory.makeCustomDatePicker("#from-date");
}

function callback() {
    "use strict";
    connectButtons();
    initiateDateFields();
}

function connectButtons() {
    "use strict";
    $('.arrows').on('click', function (evt) {
        var $arrowButton = $(evt.target).parent();
        var direction =  $arrowButton.data("direction");
        var id = $arrowButton.data("id");
        var rank = $arrowButton.parent().find(".index-number")[0];
        $.post("api/link/" + id + "/" + direction, function (data) {
            var imageInactive = '<img src="Images/arrow-' + direction + '.png">';
            var imageActive = '<img src="Images/arrow-' + direction + '-active.png">';
            if (data.success && !data.undoVote) {
                rank.innerText = data.newRank;
                $arrowButton.html(imageActive);
            } else {
                if (!data.isLoggedIn) {
                    window.location.replace('/login');
                } else if (data.undoVote) {
                    rank.innerText = data.newRank;
                    $arrowButton.html(imageInactive);
                    var oppositeDirection = direction === utils.directionDown ? utils.directionUp : utils.directionDown;
                    var oppositeButton = $arrowButton.parent().find('.' + oppositeDirection + '-arrow');
                    var oppositeImageInactive = '<img src="Images/arrow-' + oppositeDirection + '.png">';
                    oppositeButton.html(oppositeImageInactive);
                } else if (!data.alreadyVoted) {
                    window.location.replace('/error500');
                }
            }
        });

    });
    $('.blog-title-delete-btn').on('click', function (evt) {
        var $deleteButton = $(evt.target);
        var id = $deleteButton.data("blog-id");
        blogController.delete(id);
    });
}
function refresh() {
    "use strict";
    if (!filterModel.active) {
        renderComponent.loadData("api/links", callback);
    }
}

$(document).ready(function () {
    "use strict";
    renderComponent.initialize("#blog-template", "#blog-container");
    renderComponent.loadData("api/links", callback);

    filterModel.initializeRenderComponent("#blog-template", "#blog-container");
    filterModel.initialize("#to-date", "#from-date", "#search-text", callback);
    window.setInterval(refresh, 1000);
});