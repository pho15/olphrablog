/**
 * Created by rafael on 12.05.2015.
 */
var formManager = new PostFormComponent();
var validateOptions = {lang: "de"};
$(document).ready(function () {
    "use strict";
    var loginForm = $('#login-form');
    formManager.initialize(['#username', '#password'], '/auth/login');
    $('#post-login-btn').on('click', function (evt) {
        evt.preventDefault();
        loginForm.validate(validateOptions);
        if (loginForm.valid()) {
            formManager.postData('POST', function (data) {
                if (data.success) {
                    window.location.replace(data.redirectUrl);
                } else {
                    $('#error-message').html('<p>Ungültige Logindaten</p>');
                }
            });
        }
    });

    $('#cancel-login-btn').on('click', function (evt) {
        evt.preventDefault();
        window.location.replace('/');
    });
    $('#registration-login-btn').on('click', function (evt) {
        evt.preventDefault();
        window.location.replace('/registrationform');
    });

    loginForm.validate(validateOptions);
});

