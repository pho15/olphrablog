/**
 * Created by rafael on 19.05.2015.
 */
function initialize() {
    "use strict";
    $('#log-out-btn').on('click', function(evt) {
        evt.preventDefault();
        $.ajax({
            method: "DELETE",
            url: "/auth/logout"
        })
            .done(function (data) {
                if (data.success) {
                    window.location.replace('/');
                } else {
                    if (data.notLoggedIn) {
                        window.location.replace('/login');
                    }
                }
            })
            .error(function (data) {
                window.location.replace('/error500');
            });
    });
}



$(document).ready(function () {
    "use strict";
    initialize();
});