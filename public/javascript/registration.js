/**
 * Created by rafael on 12.05.2015.
 */
var formManager = new PostFormComponent();
var validateOptions = {lang: "de"};
$(document).ready(function () {
    "use strict";
    var registrationForm = $('#registration-form');
    formManager.initialize(['#username', '#password'], '/auth/register');
    $('#post-registration-btn').on('click', function (evt) {
        evt.preventDefault();
        registrationForm.validate(validateOptions);
        if (registrationForm.valid()) {
            formManager.postData('PUT', function (data) {
                if (data.success) {
                    $('#blog-registration-container').html('<p>Registriert</p>');
                    setTimeout(function () {
                        window.location.replace(data.redirectUrl);
                    }, 1000);
                } else {
                    if (data.userNameTaken) {
                        $('#error-message').html('<p>Dieser Username existiert bereits, bitte wähle einen anderen</p>');

                    } else {
                        if (data.invalidData) {
                            $('#error-message').html('<p>Da ist was bei der Validierung schief gegangen! Versuche einen andere Usernamen oder Passwort</p>');

                        } else {
                            $('#error-message').html('<p>Leider ist ein Fehler aufgetreten, versuche es später noch einmal</p>');
                        }
                    }
                }
            });
        }
    });

    $('#cancel-registration-btn').on('click', function (evt) {
        evt.preventDefault();
        window.location.replace('/');
    });
    registrationForm.validate(validateOptions);
});

