/**
 * Created by rafael on 05.05.2015.
 */
function RenderBlogComponent() {
    "use strict";
    this.compiledTemplate = "";
    this.container = "";
    this.initialize = function (templateId, containerId) {
        this.compiledTemplate = Handlebars.compile($(templateId)[0].textContent);
        this.container = $(containerId)[0];
    };
    this.renderData = function (data, callback) {
        this.container.innerHTML = this.compiledTemplate({docs: data});
        if (callback) {
            callback();
        }
    };
    this.loadData = function (url, callback) {
        var self = this;
        $.get(url, function (data) {
            self.renderData(data, callback);
        });
    };
}