var express = require('express');
var dburi = 'olphrablog';
var collections = ['posts'];
var db = require('mongojs').connect(dburi, collections);
var moment = require('moment');
var check = require('validator');
var config = require('../config');
var app = express();

db.posts.ensureIndex({
    title: "text"
});

function rank(res, id, number, action, userid) {
    "use strict";
    var data = {
        success: false,
        isLoggedIn: true,
        alreadyVoted: false,
        newRank: 0,
        undoVote: false
    };
    if (userid === undefined) {
        data.isLoggedIn = false;
        res.json(data);
    } else {
        db.posts.findOne({_id: db.ObjectId(id)}, function (err, docs) {
            if (err) {
                res.json(data);
            } else {
                var rankArray = docs.voted;
                var firstVote = true;
                var updateNeeded = false;
                var splitIndex = -1;
                var newRank;
                for (var i = 0; i < rankArray.length; i++) {
                    if (rankArray[i].user === userid && rankArray[i].vote === action) {
                        data.alreadyVoted = true;
                        firstVote = false;
                        data.newRank = parseInt(docs.rank, 10);
                        res.json(data);
                    } else if (rankArray[i].user == userid && rankArray[i].vote !== action) {
                        splitIndex = i;
                        updateNeeded = true;
                        firstVote = false;
                        data.undoVote = true;
                    }
                }
                if (firstVote || updateNeeded) {
                    newRank = parseInt(docs.rank, 10) + parseInt(number, 10);
                    if(firstVote) {
                        rankArray[rankArray.length] = {
                            user: userid,
                            vote: action
                        };
                    } else {
                        rankArray.splice(splitIndex, 1);
                    }

                    db.posts.update({_id: db.ObjectId(docs._id)}, {
                        $set: {
                            rank: newRank,
                            voted: rankArray
                        }
                    }, function (err) {
                        if (err) {
                            res.json(data)
                        }
                        data.success = true;
                        data.newRank = newRank;
                        res.json(data);
                    });
                }
            }
        });
    }
}

function insert(res, title, content, url, author) {
    "use strict";
    var data = {
        success: true,
        invalidData: false
    };
    if (check.isURL(url) && check.isLength(title, 3)) {
        var now = new Date();
        if (!content) {
            content = "";
        }
        var object = {
            title: title,
            content: content,
            url: url,
            rank: 0,
            author: author,
            date: now,
            voted: []
        };
        db.posts.insert(object, function (err, newDoc) {
            if (err) {
                data.success = false;
            } else {
                data.success = true;
            }
        });
    } else {
        data.success = false;
        data.invalidData = true;
    }
    res.json(data);
}

function remove(res, id, author, data) {
    "use strict";
    db.posts.findOne({_id: db.ObjectId(id)}, function (err, doc) {
        if (err) {
            res.json(data);
        }
        if (author !== doc.author) {
            data.isOwner = false;
            res.json(data);
        }
        db.posts.remove({_id: db.ObjectId(doc._id)}, function (err, doc) {
            if (err) {
                res.json(data);
            } else {
                data.success = true;
                res.json(data);
            }
        });
    });
}

function getAll(req, res) {
    "use strict";
    db.posts.find({}, function (err, posts) {
        if (err) {
            alert(err);
            return;
        }
        if (req.session.isLoggedIn) {
            for (var k = 0; k < posts.length; k++) {
                posts[k].isOwner = posts[k].author === req.session.username;
                if (posts[k].author === req.session.username) {
                    posts[k].author = "dir";
                }
                for(var j = 0; j < posts[k].voted.length; j++){
                    if(posts[k].voted[j].user === req.session.userId){
                        posts[k].hasVoted = true;
                        posts[k].voteDown = posts[k].voted[j].vote === config.vote.down;
                        posts[k].voteUp = posts[k].voted[j].vote === config.vote.up;
                    } else {
                        posts[k].hasVoted = false;
                    }

                }
            }
        }
        for (var i = 0; i < posts.length; i++) {
            var date = moment(posts[i].date);
            posts[i].date = date.format("DD.MM.YYYY");
        }
        res.json(posts);
    })
}


function filterBlogs(req, res, next) {
    var fDate = new Date(req.body.fromDate);
    var tDate = new Date(req.body.toDate);
    tDate.setDate(tDate.getDate() + 1);
    fDate.setDate(fDate.getDate() - 1);
    var filter = {
        date: {
            $gte: fDate,
            $lt: tDate
        }
    };
    if (req.body.searchText !== "") {
        filter = {
            "$text": {
                "$search": req.body.searchText
            },
            date: {
                $gte: fDate,
                $lt: tDate
            }
        };
    }
    db.posts.find(filter,  function (err, posts) {
            if (err) {
                error(err);
                return;
            }
            if (req.session.isLoggedIn) {
                for (var k = 0; k < posts.length; k++) {
                    posts[k].isOwner = posts[k].author === req.session.username;
                    if (posts[k].author === req.session.username) {
                        posts[k].author = "dir";
                    }
                    for(var j = 0; j < posts[k].voted.length; j++){
                        if(posts[k].voted[j].user === req.session.userId){
                            posts[k].hasVoted = true;
                            posts[k].voteDown = posts[k].voted[j].vote === config.vote.down;
                            posts[k].voteUp = posts[k].voted[j].vote === config.vote.up;
                        } else {
                            posts[k].hasVoted = false;
                        }
                    }
                }
            }
            for (var i = 0; i < posts.length; i++) {
                var date = moment(posts[i].date);
                posts[i].date = date.format("DD.MM.YYYY");
            }
            res.json(posts);
        }

    )
}
function getLatestDate(res) {
    db.posts.find().sort({lastModifiedDate:1}, function(err, docs) {
        if(err) {
            alert(err);
            return;
        }
        if(docs.length < 1) {
            res.json(new Date());
        } else {
            res.json(docs[0].date);
        }
    })
}
function upgradeDatabase(res){
    db.posts.find({}, function(err, docs){
        if(err){
            console.error(err);
            res.json(false);
        }
        for(var i = 0; i < docs.length; i++){
            db.posts.update({_id: docs[i]._id}, {$set: {voted: []}}, function(err){
                if(err){
                    console.error(err)
                }
            });
            setTimeout("", 100);
        }
        res.json(true)
    })
}
//noinspection JSUnresolvedFunction
app.put('/link', function (req, res) {
    res.setHeader('Content-Type', 'text/json');
    insert(res, req.body.title, req.body.content, req.body.url, req.session.username);
});
app.delete('/:id', function (req, res) {
    res.setHeader('Content-Type', 'text/json');
    var data = {
        success: false,
        isOwner: true,
        isLoggedIn: false
    };
    if (!req.session.isLoggedIn) {
        res.json(data);
    }
    data.isLoggedIn = true;
    remove(res, req.params.id, req.session.username, data);
});
app.get('/links', function (req, res) {
    res.setHeader('Content-Type', 'text/json');
    getAll(req, res);
});
app.post('/link/:id/up', function (req, res, next) {
    rank(res, req.params.id, "1", config.vote.up, req.session.userId);
});
app.post('/link/:id/down', function (req, res, next) {
    rank(res, req.params.id, "-1", config.vote.down, req.session.userId);
});
app.post("/filtered-links", function (req, res, next) {
    filterBlogs(req, res, next);
});
app.get("/date/oldest", function (req, res) {
   res.setHeader("Content-Type", 'text/json');
   getLatestDate(res);
});

app.get('/db/upgrade', function(req, res){
    upgradeDatabase(res);
});
app.get('/db/purge', function(req, res){
    db.posts.remove({}, function(err){
        if(err){
            console.error(err);
        }
        res.json(true);
    })
});
module.exports = app;