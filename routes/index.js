var express = require('express');
var config = require('../config');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    "use strict";
    var variables = {
        isLoggedIn: false,
        location: req.session.location,
        config: config
    };
    if (req.session.isLoggedIn) {
        variables.isLoggedIn = true;
        variables.username = req.session.username;
    }
    res.render('index', {pageData: variables});
});
router.get('/blogform', function(req, res, next) {
    "use strict";
    var variables = {
        location: req.session.location,
        config: config,
        isLoggedIn: false
    };
    if (req.session.isLoggedIn) {
        variables.isLoggedIn = true;
        variables.username = req.session.username;
    } else {
        res.redirect('/login');
    }
    res.render('blogform', {pageData: variables});
});

router.get('/registrationform', function(req, res, next) {
    "use strict";
    var variables = {
        location: req.session.location,
        config: config,
        isLoggedIn: false
    };
    if (req.session.isLoggedIn) {
        variables.isLoggedIn = true;
        variables.username = req.session.username;
    }
    res.render('registration', {pageData: variables});
});
router.get('/login', function(req, res, next) {
    "use strict";
    var variables = {
        location: req.session.location,
        config: config,
        isLoggedIn: false
    };
    if (req.session.isLoggedIn) {
        variables.isLoggedIn = true;
        variables.username = req.session.username;
    }
    res.render('login', {pageData: variables});
});
router.get('/error500', function(req, res, next) {
    "use strict";
    res.render('error500');
});
router.get('/error404', function(req, res, next) {
    "use strict";
    res.render('error404');
});
module.exports = router;